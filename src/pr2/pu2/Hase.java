package pr2.pu2;

public class Hase extends Tier {
	private static int ic = 1;

	protected Hase(int ihp) {
		super(40);

	}

	// neue create()
	public static Hase create() {

		Hase neu = new Hase(random());
		neu.setName();
		neu.start();
		// System.out.println(ic);

		ic++;
		return neu;
	}

	// neuer Name
	protected void setName() {

		if (ic < 10) {
			name = ("Hase-" + "00" + ic);
		} else if (ic > 9 && ic < 100) {
			name = ("Hase-" + "0" + ic);
		} else if (ic > 99) {
			name = ("Hase-" + ic);
		}
		// System.out.println(hp);
	}

}
