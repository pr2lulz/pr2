package pr2.pu2;

import java.util.*;

public class AnimalComparator<T extends Tier> implements Comparator<T>{

	// einfacher Komparator
	public int compare(T t1,T t2) {

		return t1.getHp()-t2.getHp();

	}

}
