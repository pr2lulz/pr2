package pr2.pu2;

public class Zebra extends Tier {
	private static int ic = 1;

	protected Zebra(int ihp) {
		super(ihp);
	}

	public static Zebra create() {

		Zebra neu = new Zebra(random());
		neu.setName();
		neu.start();

		ic++;
		return neu;
	}

	protected void setName() {

		if (ic < 10) {
			name = ("Zebra-" + "00" + ic);
		} else if (ic > 9 && ic < 100) {
			name = ("Zebra-" + "0" + ic);
		} else if (ic > 99) {
			name = ("Zebra-" + ic);
		}

	}
}
