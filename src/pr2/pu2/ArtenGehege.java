package pr2.pu2;

import java.util.*;

// T ist jedes beliebiges Tier
public class ArtenGehege<T extends Tier> {
	
	//Set und Name anlegen
	Set<T> set;
	String gName;

	public ArtenGehege(String iName) {
		this.gName = iName;
		this.set = new HashSet<T>();

	}
	//Hinzuf�gen von Tier T
	public void einsperren(T Input) {

		this.set.add(Input);

		System.out.println("Eingesperrt: " + Input.getName2());

	}
	
	public void corona(int p) {
		
		Iterator<? extends Tier> o = set.iterator();
		
		while(o.hasNext()) {
			Tier current = o.next();
			
			int m = (int) (Math.random() * 100);
			
			if (m <= p) {
				//kill(current);
				current.hp = 0;
				System.out.println("Tier is tot");
			
		}}
	}
	
	//Compilerfehler, wenn ein falsches Tier ins Gehege soll
	public static void main(String[] args) {

		ArtenGehege<Hase> g = new ArtenGehege<Hase>("hallo");

		// g.einsperren(Tier.create());
		g.einsperren(Hase.create());
		g.einsperren(Hase.create());
		g.einsperren(Hase.create());
		g.corona(20);


	}
	// Aufgabe 6: Man sollte den Collection-Typ Set w�hlen, da dieser als einziger
	// Typ keine Duplikate erlaubt.
	// Es kann ja in der realit�t nicht 2-mal das gleiche Tier existieren.

}
