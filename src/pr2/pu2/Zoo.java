package pr2.pu2;

import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

public class Zoo<T extends ArtenGehege> {

	public static void main(String[] args) {

		//TreeMap anlegen (HashMap geht auch, Sortierung �ndert sich nur marginal)
		TreeMap<String, ArtenGehege> Zoo1 = new TreeMap<String, ArtenGehege>();
		
		//Gehege anlegen
		ArtenGehege<Hase> Hasenstall = new ArtenGehege<Hase>("Hasenstall");
		ArtenGehege<Hund> Hundek�fig = new ArtenGehege<Hund>("Hundek�fig");
		ArtenGehege<Zebra> Zebraweide = new ArtenGehege<Zebra>("Zebraweide");

		//Gehege dem Zoo hinzuf�gen
		Zoo1.put("Hasenstall", Hasenstall);
		Zoo1.put("Hundek�fig", Hundek�fig);
		Zoo1.put("Zebraweide", Zebraweide);

		//Tiere einsperren
		Hasenstall.einsperren(Hase.create());
		Hasenstall.einsperren(Hase.create());
		Hasenstall.einsperren(Hase.create());
		// Hasenstall.einsperren(Tier.create());

		Hundek�fig.einsperren(Hund.create());
		Hundek�fig.einsperren(Hund.create());

		Zebraweide.einsperren(Zebra.create());
		Zebraweide.einsperren(Zebra.create());
		Zebraweide.einsperren(Zebra.create());

		
		//Iterator f�r Gehege
		Iterator<Entry<String, ArtenGehege>> i = Zoo1.entrySet().iterator();
		
		//Iterator f�r Tiere
		Iterator<? extends Tier> h = i.next().getValue().set.iterator();
		
		//Liste f�r sortiere Ausgabe
		List<Tier> l = new ArrayList<Tier>();

		//Doppelte Iteration durch 2 verschachtelte While-Schleifen + direkte R�ckgabe + Eintrag in sortierte Liste
		int counter = 0;
		while (counter != Zoo1.size()) {
			counter++;
			while (h.hasNext()) {
				Tier current = h.next();
				System.out.println("R�ckgabe: " + current.toString());
				l.add(current);
				
			}
			//Neusetzung von h f�r letztes Set
			if (counter != Zoo1.size())
				h = i.next().getValue().set.iterator();
		}
		
		//Sortiervorgang mit eigenem Komparator
		Collections.sort(l, new AnimalComparator<Tier>());

		//Ausgabe der sortierten Liste l
		for (Tier t : l) {
			System.out.println("Sortierte R�ckgabe: " + t.toString());
		}

	}

}
