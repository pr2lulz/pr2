package pr2.pu2;

import java.io.*;
import java.util.Scanner;

public class Copy {

	public static void main(String[] args) throws IOException {

		//Anlegen eines Scanners f�r Texteingabe
		Scanner sc = new Scanner(System.in);
		
		// Einlesen der Eingabe
		System.out.println("Quelle?");
		String InputString = sc.nextLine();
		System.out.println("Ziel?");
		String OutputString = sc.nextLine();

		// Beispiele:
		// "/Users/DELLO/Desktop/Ziel.txt"
		// "/Users/DELLO/Desktop/Quelle.txt"

		copy(InputString, OutputString, true, false);

	}

	public static void copy(File fin, File fout, boolean append) throws IOException {
		int len = 32768;
		byte[] buff = new byte[(int) Math.min(len, fin.length())];
		FileInputStream fis = null;
		FileOutputStream fos = null;
		try {
			fis = new FileInputStream(fin);
			fos = new FileOutputStream(fout, append);
			while (0 < (len = fis.read(buff)))
				fos.write(buff, 0, len);
		} finally {
			try {
				if (fos != null)
					fos.close();
			} catch (IOException ex) {
				/* ok */}
			try {
				if (fis != null)
					fis.close();
			} catch (IOException ex) {
				/* ok */}
		}
	}

	public static void copy(String fromFileName, String toFileName, boolean overwriteIfExists, boolean append)
			throws IOException {
		
		File fromFile = new File(fromFileName);
		File toFile = new File(toFileName);
		
		//Abfangen von Fehlerquellen
		if (!fromFile.exists())
			throw new IOException("Fehler: Quelldatei fehlt: " + fromFileName);
		if (!fromFile.isFile())
			throw new IOException("Fehler: Quelle ist ein Verzeichnis: " + fromFileName);
		if (!fromFile.canRead())
			throw new IOException("Fehler: Keine Leseberechtigung fuer Quelldatei: " + fromFileName);

		if (toFile.isDirectory())
			toFile = new File(toFile, fromFile.getName());

		if (toFile.exists()) {
			if (!overwriteIfExists && !append)
				throw new IOException("Fehler: Zieldatei existiert bereits: " + toFileName);
			if (!toFile.canWrite())
				throw new IOException("Fehler: Keine Schreibberechtigung fuer Zieldatei: " + toFileName);
		} else {
			String parent = toFile.getParent();
			if (parent == null)
				parent = System.getProperty("user.dir");
			File dir = new File(parent);
			if (!dir.exists())
				throw new IOException("Fehler: Zielverzeichnis existiert nicht: " + parent);
			if (dir.isFile())
				throw new IOException("Fehler: Ziel ist kein Verzeichnis: " + parent);
			if (!dir.canWrite())
				throw new IOException("Fehler: Keine Schreibberechtigung fuer Zielverzeichnis: " + parent);
		}

		copy(fromFile, toFile, append);
	}

}
