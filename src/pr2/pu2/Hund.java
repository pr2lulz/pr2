package pr2.pu2;

public class Hund extends Tier {
	private static int ic = 1;

	protected Hund(int ihp) {
		super(ihp);

	}

	public static Hund create() {

		Hund neu = new Hund(random());
		neu.setName();
		neu.start();
		// System.out.println(ic);

		ic++;
		return neu;
	}

	protected void setName() {

		if (ic < 10) {
			name = ("Hund-" + "00" + ic);
		} else if (ic > 9 && ic < 100) {
			name = ("Hund-" + "0" + ic);
		} else if (ic > 99) {
			name = ("Hund-" + ic);
		}
		// System.out.println(hp);
	}
}
