package pr2.pu2;

public class Tier extends Thread {

	protected int x;
	protected int y;
	protected int hp;
	protected static int ic = 1;
	protected String name;

	protected Tier(int ihp) {

		this.hp = ihp;

	}
	//�berschreibe die run() und f�ge einen Lebenszyklus + move() ein
	@Override
	public void run() {
		System.out.println("Starte Thread: " + name);
		do {
			move();
			hp--;
			//System.out.println(hp);
			try {
				Thread.sleep(100);
				
			} catch (InterruptedException e) {
				System.out.println("Tier kann nicht schlafen!");
				e.printStackTrace();
			}
		} while (hp > 0);
		
		System.out.println("Beende Thread: " + name);
		this.stop();

	}

	protected Thread getThread() {
		return currentThread();
	}

	protected String getThreadName() {
		return getName();
	}
	
	//einfachster Zufallsgenerator
	protected static int random() {

		return (int) (Math.random() * 100);
	}
	//create() mit integrierter run() und zuf�llig vielen Hp
	public static Tier create() {

		Tier neu = new Tier(random());
		neu.setName();
		neu.run();
		ic++;
		return neu;
	}
	//Namensgebung/ Nummerierung
	protected void setName() {

		if (ic < 10) {
			name = ("Tier-" + "00" + ic);
		} else if (ic > 9 && ic < 100) {
			name = ("Tier-" + "0" + ic);
		} else if (ic > 99) {
			name = ("Tier-" + ic);
		}
	}
	//gibt den tats�chlichen Tiernamen zur�ck + getName soll nicht �berschrieben werden
	protected String getName2() {
		return this.name;
	}
	
	//Hp Getter
	protected int getHp() {
		return this.hp;
	}
	
	//random Bewegung durch move()
	public void move() {
		int m = (int) (Math.random() * 5);
		if (m == 0) {
			this.x++;
		} else if (m == 1) {
			this.x--;
		} else if (m == 2) {
			this.y++;
		} else if (m == 3) {
			this.y--;
		} else if (m == 4) {

		}
	}

	//Gibt Name und Hp des Tiers aus
	@Override
	public String toString() {
		return this.name + " (" + this.hp + ")";
	}
	public static void kill(Tier t){
		System.out.println("Gewaltsamer Schlaf: Thread: " + t.getState().toString());
		t.interrupt();
		t.stop();//????
	}
	

	public static void main(String[] args) {
		 Tier Doggo = new Tier(33);
		 Doggo.start();
		// kill(Doggo);
		// create();
		// Hase.create();
		// create();
		// create();

	}

}
