package pu1;

public class KomplexeZahlKlein2 extends KomplexeZahl {

	protected double reel, imag;

	public KomplexeZahlKlein2(double inputR, double inputI) throws KZKException {
		super(inputR, inputI);

		if (inputI > 10) {

			throw new KZKException("Der imagin�re Anteil liegt �ber 10!" + " Der reelle Anteil betr�gt: " + this.re());

		}

	}

	// Exception als Subklasse
	class KZKException extends Exception {

		private String info;

		public KZKException() {
			super("Der imagin�re Anteil liegt �ber 10!" + " Der reelle Anteil betr�gt: " + reel);
		}

		public KZKException(String message) {
			super(message);
			this.info = message;
		}

		public String getMessage() {
			return this.info;
		}
	}
}
