package pu1;

public class KomplexeZahlKlein1 extends KomplexeZahl {

	protected double reel;
	protected double imag;

	// Konstruktor mit anschlie�ender �berpr�fung der imagin�ren Zahl
	public KomplexeZahlKlein1(double inputR, double inputI) throws Exception {
		super(inputR, inputI);

		if (inputI > 10) {

			throw new Exception("Der imagin�re Anteil liegt �ber 10!" + " Der reelle Anteil betr�gt: " + this.re());
		}

	}
}
