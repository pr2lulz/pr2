package pu1;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;


class JunitTestat {

	@Test
	void testadd() {
		KomplexeZahl a = new KomplexeZahl(2,5);
		KomplexeZahl b = new KomplexeZahl(3,7);
		assertEquals("5.0 + i*12.0", a.add(b).toString());
	}
	@Test
	void testadd2() {
		KomplexeZahl a = new KomplexeZahl(2,5);
		KomplexeZahl b = new KomplexeZahl(3,7);
		assertEquals("5.0 + i*12.0", KomplexeZahl.add2(a, b).toString());
	}
	@Test
	void testprod() {
		KomplexeZahl a = new KomplexeZahl(2,5);
		KomplexeZahl b = new KomplexeZahl(3,7);
		assertEquals("-29.0 + i*29.0", a.prod(b).toString());
	}
	@Test
	void testprod2() {
		KomplexeZahl a = new KomplexeZahl(2,5);
		KomplexeZahl b = new KomplexeZahl(3,7);
		assertEquals("-29.0 + i*29.0", KomplexeZahl.prod2(a, b).toString());
	}
}
