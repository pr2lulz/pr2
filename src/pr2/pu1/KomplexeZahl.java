package pu1;

public class KomplexeZahl {

	protected double reel, imag;

	public KomplexeZahl(double inputR, double inputI) {

		this.reel = inputR;
		this.imag = inputI;

	}

	// neue toString anlegen
	public String toString() {
		String Cz = new String(reel + " + i*" + imag );
		return Cz;
	}

	// add als nicht statisch
	protected KomplexeZahl add(KomplexeZahl n) {
		return new KomplexeZahl(reel + n.reel, imag + n.imag);
	}

	// add2 als statisch
	protected static KomplexeZahl add2(KomplexeZahl a, KomplexeZahl b) {

		return new KomplexeZahl(a.reel + b.reel, a.imag + b.imag);

	}

	// prod als nicht statisch
	protected KomplexeZahl prod(KomplexeZahl n) {
		return new KomplexeZahl(reel * n.reel - imag * n.imag, reel * n.imag + imag * n.reel);
	}

	// Prod2 als statisch
	protected static KomplexeZahl prod2(KomplexeZahl a, KomplexeZahl b) {

		return new KomplexeZahl(a.reel * b.reel - a.imag * b.imag, a.reel * b.imag + a.imag * b.reel);

	}

	protected double re() {
		return this.reel;
	}

	protected double im() {
		return imag;
	}

	public static void main(String[] args) throws KomplexeZahlKlein2.KZKException, Exception {

		KomplexeZahl x = new KomplexeZahl(2.5, 4.5);
		
		System.out.println("x = " + x);
		
		KomplexeZahl a = new KomplexeZahl(5, 8);
		KomplexeZahl b = new KomplexeZahl(3, 4);

		System.out.println("a = " + a);
		System.out.println("b = " + b);
		System.out.println("a + b = " + KomplexeZahl.add2(a ,b));

		ReelleZahl k = new ReelleZahl(3, 10);
		System.out.println(k);

		KomplexeZahlKlein1 m = new KomplexeZahlKlein1(3, 2);
		System.out.println("m = " + m);

		KomplexeZahlKlein2 n = new KomplexeZahlKlein2(3, 11);
		System.out.println("n = " + n);

	}
}
