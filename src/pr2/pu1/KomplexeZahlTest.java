package pu1;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

public class KomplexeZahlTest {

	@Test
	public void testAdd() {
		KomplexeZahl t = new KomplexeZahl(2, 3.5);
		KomplexeZahl q = new KomplexeZahl(4, 3.5);
		assertEquals("6.0 + i*7.0", t.add(q).toString());
	}

	@Test
	public void testKZK() {
		try {
			KomplexeZahlKlein2 j = new KomplexeZahlKlein2(10, 12);

		} catch (KomplexeZahlKlein2.KZKException ex) {
			assertTrue(true);
			
		}

	}
	@Test
	public void testRe() {
		KomplexeZahl u = new KomplexeZahl(2,5);
		assertEquals(2,u.re());
	}
	
	@Test
	public void testEx() {
		try {
			KomplexeZahlKlein1 n = new KomplexeZahlKlein1(2,12);
			
		}catch(Exception ex2) {
			assertTrue(true);
		}
		
	}
	
	@Test(expected = KomplexeZahlKlein2.KZKException.class)
	void testNew() {
		
	}
}
