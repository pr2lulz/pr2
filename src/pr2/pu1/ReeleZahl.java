package pr2.pu1;

public class ReeleZahl extends KomplexeZahl {

    public ReeleZahl(double re) {
        super(re, 0.0);
    }

    public String toString() {
        return String.valueOf(this.re);
    }

}